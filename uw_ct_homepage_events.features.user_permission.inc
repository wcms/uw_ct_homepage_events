<?php
/**
 * @file
 * uw_ct_homepage_events.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_events_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_events content'.
  $permissions['create homepage_events content'] = array(
    'name' => 'create homepage_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_events content'.
  $permissions['delete any homepage_events content'] = array(
    'name' => 'delete any homepage_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_events content'.
  $permissions['delete own homepage_events content'] = array(
    'name' => 'delete own homepage_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any homepage_events content'.
  $permissions['edit any homepage_events content'] = array(
    'name' => 'edit any homepage_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_events content'.
  $permissions['edit own homepage_events content'] = array(
    'name' => 'edit own homepage_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter homepage_events revision log entry'.
  $permissions['enter homepage_events revision log entry'] = array(
    'name' => 'enter homepage_events revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events authored by option'.
  $permissions['override homepage_events authored by option'] = array(
    'name' => 'override homepage_events authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events authored on option'.
  $permissions['override homepage_events authored on option'] = array(
    'name' => 'override homepage_events authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events promote to front page option'.
  $permissions['override homepage_events promote to front page option'] = array(
    'name' => 'override homepage_events promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events published option'.
  $permissions['override homepage_events published option'] = array(
    'name' => 'override homepage_events published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events revision option'.
  $permissions['override homepage_events revision option'] = array(
    'name' => 'override homepage_events revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_events sticky option'.
  $permissions['override homepage_events sticky option'] = array(
    'name' => 'override homepage_events sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
