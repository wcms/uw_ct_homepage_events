<?php
/**
 * @file
 * uw_ct_homepage_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_homepage_events_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_events_node_info() {
  $items = array(
    'homepage_events' => array(
      'name' => t('Homepage Events'),
      'base' => 'node_content',
      'description' => t('Each homepage event links to an event within or external to the Waterloo web space.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
